'use strict';

angular.module('myApp.viewaddreminder', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewaddreminder', {
            templateUrl: 'viewaddreminder/viewaddreminder.html',
            controller: 'viewaddreminderCtrl'
        });
    }])

    .controller('viewaddreminderCtrl', ['$http', '$rootScope', '$window','$routeParams', function ($http, $rootScope, $window, $routeParams) {
        var URL = 'http://localhost:8080';
        var self = this;
        this.createreminder = {
            'reminder_note': '',
            'contact_date': ''
        };
        self.choosenClient = $routeParams.choosenClient;

        this.sendToBackend = function () {
            var url = "remindernote=" + self.createreminder.reminder_note;
            var url1 = "&contactdate=" + document.getElementById("contact_date").value;

            console.log("Request: " + URL + "/reminders/createreminder?" + url + url1);
            $http.get(URL + "/reminders/createreminder?" + url + url1)
                .then(function (response) {
                    console.log(response.data.message);
                    $http.get(URL+"/reminders/addreminder?clientid=" + self.choosenClient + "&reminderid="+response.data.message).then(
                        function (response) {
                            console.log("Success: " + response);
                            $window.location = "/#!/showclientinfo?choosenClient=" + self.choosenClient;
                        },
                        function (response) {
                            console.log("Error: " + response);
                        }
                    );
                }, function (data) {
                    console.log(data);
                });
        };
    }]);