'use strict';
angular.module('myApp.home', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/home', {
            templateUrl: 'home/home.html',
            controller: 'homeCtrl'
        });
    }])
    .controller('homeCtrl', ['$http', function ($http) {
        var URL = 'http://localhost:8080';
        var self = this;

        this.home = function() {
            $http.get(URL + '/home/home')
        }
    }]);