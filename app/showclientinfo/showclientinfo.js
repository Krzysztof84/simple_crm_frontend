'use strict';
angular.module('myApp.showclientinfo', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/showclientinfo', {
            templateUrl: 'showclientinfo/showclientinfo.html',
            controller: 'showclientinfoCtrl'
        });
    }])
    .controller('showclientinfoCtrl', ['$http', '$rootScope', '$routeParams', function ($http , $rootScope, $routeParams) {
        var URL = 'http://localhost:8080';
        var self = this;
        this.clientCases = [];
        this.clientInfo = {};
        this.clientReminders = [];
        this.choosenClient = $routeParams.choosenClient;

        this.fetchClients = function () {
            $http.get(URL + '/clients/clientinfo?clientid=' + self.choosenClient)
                .then(
                    function (data) {
                        console.log(data);
                        self.clientInfo = data.data;

                        self.clientCases = [];


                        for (var index in self.clientInfo.crmCaseList) {
                            console.log(self.clientInfo.crmCaseList[index]);
                            self.clientCases.push(self.clientInfo.crmCaseList[index]);
                        }

                    },
                    function () {
                        console.log("error");
                    }
                );
        };
        self.fetchClients();


        this.fetchClients = function () {
            $http.get(URL + '/clients/clientinfo?clientid=' + self.choosenClient)
                .then(
                    function (data) {
                        console.log(data);
                        self.clientInfo = data.data;

                        self.clientReminders = [];


                        for (var index in self.clientInfo) {
                            console.log(self.clientInfo.crmReminderList[index]);
                            self.clientReminders.push(self.clientInfo.crmReminderList[index]);
                        }

                    },
                    function () {
                        console.log("error");
                    }
                );
        };
        self.fetchClients();

    }]);