'use strict';
angular.module('myApp.view1', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view1', {
            templateUrl: 'view1/view1.html',
            controller: 'View1Ctrl'
        });
    }])
    .controller('View1Ctrl', ['$http', '$rootScope', 'AuthService', function ($http , $rootScope, AuthService) {
        var URL = 'http://localhost:8080';
        var self = this;

        this.userList = [];

        this.loggedInUser = AuthService.loggedInUser.appUserId;

        this.fetchUsers = function () {
            $http.get(URL + '/user/list')
                .then(
                    function (data) {
                        console.log(data);
                        var users = data.data.objects;

                        self.userList = [];

                        for (var index in users) {
                            console.log(users[index]);
                            self.userList.push(users[index]);
                        }
                    },
                    function () {
                        console.log("error");
                    }
                );
        };
        self.fetchUsers();
    }]);