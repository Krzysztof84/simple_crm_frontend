'use strict';

angular.module('myApp.viewaddcase', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewaddcase', {
            templateUrl: 'viewaddcase/viewaddcase.html',
            controller: 'viewaddcaseCtrl'
        });
    }])

    .controller('viewaddcaseCtrl', ['$http', '$rootScope', '$window','$routeParams', function ($http, $rootScope, $window, $routeParams) {
        var URL = 'http://localhost:8080';
        var self = this;
        this.createCase = {
            'case_description': ''
        };
        self.choosenClient = $routeParams.choosenClient;

        this.sendToBackend = function () {
            var url = "casedescription=" + self.createCase.case_description;

            console.log("Request: " + URL + "/case/createcase?" + url);
            $http.get(URL + "/case/createcase?" + url)
                .then(function (response) {
                    console.log(response.data.message);
                    $http.get(URL+"/case/addcase?clientid=" + self.choosenClient + "&caseid="+response.data.message).then(
                        function (response) {
                            console.log("Success: " + response);
                            $window.location = "/#!/showclientinfo?choosenClient=" + self.choosenClient;
                        },
                        function (response) {
                            console.log("Error: " + response);
                        }
                    );
                }, function (data) {
                    console.log(data);
                });
        };
    }]);